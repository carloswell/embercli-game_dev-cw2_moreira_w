import Component from '@ember/component';  //import ember framework for the game.

function check_game_winner(state){      //function called when the check_winner counter is playing.
    var patterns = [       // patterns array variable to hold the values for horizontal,vertical and diagonal partterns for winning
        
      //horizontal win partner

      //1-horizontal row
      [[0,0], [1,0], [2,0], [3,0]],
      [[1,0], [2,0], [3,0], [4,0]],
      [[2,0], [3,0], [4,0], [5,0]],
      [[3,0], [4,0], [5,0], [6,0]],
      //2-horizontal row
      [[0,1], [1,1], [2,1], [3,1]],
      [[1,1], [2,1], [3,1], [4,1]],
      [[2,1], [3,1], [4,1], [5,1]],
      [[3,1], [4,1], [5,1], [6,1]],
      //3-horizontal row
      [[0,2], [1,2], [2,2], [3,2]],
      [[1,2], [2,2], [3,2], [4,2]],
      [[2,2], [3,2], [4,2], [5,2]],
      [[3,2], [4,2], [5,2], [6,2]],
      //4-horizontal row
      [[0,3], [1,3], [2,3], [3,3]],
      [[1,3], [2,3], [3,3], [4,3]],
      [[2,3], [3,3], [4,3], [5,3]],
      [[3,3], [4,3], [5,3], [6,3]],
      //5-horizontal row
      [[0,4], [1,4], [2,4], [3,4]],
      [[1,4], [2,4], [3,4], [4,4]],
      [[2,4], [3,4], [4,4], [5,4]],
      [[3,4], [4,4], [5,4], [6,4]],
      //6- horizontal row
      [[0,5], [1,5], [2,5], [3,5]],
      [[1,5], [2,5], [3,5], [4,5]],
      [[2,5], [3,5], [4,5], [5,5]],
      [[3,5], [4,5], [5,5], [6,5]],

      //vertical win partner

      //1-vertical column  
      [[0,0], [0,1], [0,2], [0,3]],
      [[0,1], [0,2], [0,3], [0,4]],
      [[0,2], [0,3], [0,4], [0,5]],
      //2-vertical column
      [[1,0], [1,1], [1,2], [1,3]],
      [[1,1], [1,2], [1,3], [1,4]],
      [[1,2], [1,3], [1,4], [1,5]],
      //3-vertical column
      [[2,0], [2,1], [2,2], [2,3]],
      [[2,1], [2,2], [2,3], [2,4]],
      [[2,2], [2,3], [2,4], [2,5]],
      //4-vertical column
      [[3,0], [3,1], [3,2], [3,3]],
      [[3,1], [3,2], [3,3], [3,4]],
      [[3,2], [3,3], [3,4], [3,5]],
      //5-vertical column
      [[4,0], [4,1], [4,2], [4,3]],
      [[4,1], [4,2], [4,3], [4,4]],
      [[4,2], [4,3], [4,4], [4,5]],
      //6-vertical column
      [[5,0], [5,1], [5,2], [5,3]],
      [[5,1], [5,2], [5,3], [5,4]],
      [[5,2], [5,3], [5,4], [5,5]],
      //7-vertical column
      [[6,0], [6,1], [6,2], [6,3]],
      [[6,1], [6,2], [6,3], [6,4]],
      [[6,2], [6,3], [6,4], [6,5]],

      // Diagonal pattern wins   

      //Left to right diagonal..
      [[0,3], [1,2], [2,1], [3,0]],
      [[0,4], [1,3], [2,2], [3,1]],
      [[1,4], [2,3], [3,2], [4,1]],
      [[1,3], [2,2], [3,1], [4,0]],
      [[0,5], [1,4], [2,3], [3,2]],
      [[1,4], [2,3], [3,2], [4,1]],
      [[2,3], [3,2], [4,1], [5,0]],
      [[1,5], [2,4], [3,3], [4,2]],
      [[2,4], [3,3], [4,2], [5,1]],
      [[3,3], [4,2], [5,1], [6,0]],
      [[2,5], [3,4], [4,3], [5,2]],
      [[3,4], [4,3], [5,2], [6,1]],
      [[3,5], [4,4], [5,3], [6,2]],
      //right to left diagonal
      [[3,0], [4,1], [5,2], [6,3]],
      [[2,0], [3,1], [4,2], [5,3]],
      [[3,1], [4,2], [5,3], [6,4]],
      [[1,0], [2,1], [3,2], [4,3]],
      [[2,1], [3,2], [4,3], [5,4]],
      [[3,2], [4,3], [5,4], [6,5]],
      [[0,0], [1,1], [2,2], [3,3]],
      [[1,1], [2,2], [3,3], [4,4]],
      [[2,2], [3,3], [4,4], [5,5]],
      [[0,1], [1,2], [2,3], [3,4]],
      [[1,2], [2,3], [3,4], [4,5]],
      [[0,2], [1,3], [2,4], [3,5]],

    ];
        //for loop is responsible to loope over the patterns arrays searching for a winner

    for(var pidx = 0; pidx < patterns.length; pidx++){
        var pattern = patterns[pidx];
        var winner = state[pattern[0][0]][pattern[0][1]];    //check the state on begging of each pattern to make sure eash square has the same state.
        if(winner){    // if there is a winner return winner otherwise
            for(var idx =1; idx < pattern.length; idx ++){
                if(winner != state[pattern[idx][0]][pattern[idx][1]]) {
                    winner = undefined;       //if not winner break and start again.
                    break;
                }
            }
            if(winner) {
                return winner;
            }
        }
    }  
        // varaible to hold the value of game draw

        var draw = true;
        for(var x = 0; x <= 2; x++){   //looping by each square check if none of them are empty
            for(var y = 0; y <= 2; y++){
                if(!state[x][y]) {      //if is not empty game continue otherwise return to check_winner and the message. 
                  return undefined;
                }
            }
        }
        return '';    
}//end of check_game_winner function

// function responsible to use heuristic algorithm, helping the computer pruning the squares and make his moves more inteligent
function alphabeta(state, limit, player, alpha, beta){
    var moves = []
    if(limit > 0) {   //if statement to get parameter limit and if the same is greater than 0.

        if(player === 'o') {   //if computer is the player find minimum and maximo value on alpahbeta pruning
            var cutoff = Number.MIN_VALUE;
        } else {
            var cutoff = Number.MAX_VALUE;
        }
        for(var idx2 = 5; idx2 >= 0; idx2--) {    // nested for looping to find the squares that are empty and add as possible move
            for(var idx1 = 0; idx1 <= 6; idx1++){
                if(state[idx1][idx2] === undefined) {
                    var move = {
                        x: idx1,
                        y: idx2,
                        state: deepClone(state),
                        score: 0
                    };
                    move.state[idx1][idx2] = player;    //The move created by taked the coordentates is passed to deepClone function to store and make used of this move further.

                        if(limit === 1 || check_game_winner(move.state) !== undefined) {
                            if(check_game_winner(move.state) !== undefined){
                                var winner = check_game_winner(move.state);
                                if(winner === 'o') {     //If winner is the computer the score is set to 1000
                                    move.score = 1000;
                                }else if(winner === 'x'){   //if winner is the user player the score is set to -1000
                                    move.score = -1000;
                                }
                            }

                        } else{    //otherwise loop again and with if statement to come up with other score after a move has be done 
                            move.moves = alphabeta(move.state, limit - 1, player == 'x' ? 'o' : 'x', alpha, beta);
                            var score = undefined;
                            for(var idx3 = 0; idx3 < move.moves.length; idx3++){
                                if(score === undefined){
                                    score = move.moves[idx3].score;
                                } else if(player === 'x') {
                                    score = Math.max(score, move.moves[idx3].score);
                                } else if(player === 'o') {
                                    score = Math.min(score, move.moves[idx3].score);
                                }
                            }
                            move.score = score;

                        }
                        
                     moves.push(move);   //after moves score calculated push move back to computer_move function to store possible moves.
                     if(player === '0'){   // if statement to calcualte min as beta and max as alpha . 
                         cutoff = Math.max(cutoff, move.score);
                         alpha = Math.max(cutofff, alpha);
                     }else {
                         cutoff = Math.min(cutoff, move.score);
                         beta = Math.min(cutoff, beta);
                     }
                     if(beta <= alpha){   //if beta is less or equal to alpha return moves.
                         //return moves;
                     }
                }
            }
        }
    }
    return moves;   //return moves to deepClone function.
};

//Function responsible to calculate the computer moves make proactive.

function computer_move(state) {
  
    var moves = alphabeta(state, 2, 'o', Number.MIN_VALUE, Number.MAX_VALUE);  //change the limit on alphabeta function  making the computer hard to bit.
    var max_score = undefined;
    var move = undefined;

    for(var idx = 0; idx < moves.length; idx++) {  //for loop to looping over the possible scores to find highest and pass the move
        if(max_score === undefined || moves[idx].score > max_score) {
            max_score = moves[idx].score;
            move = {
                x:moves[idx].x,   //set coordentates x for the score  and pas to the counter
                y:moves[idx].y     //set coordentates y for the score  and pas to the counter
            }
        }
    }
    return move;  //return move to deepClone()
}//end of computer_move()


/*DeepClone function is responsible to store the game trees by use of for loop 
*and story inside a array, to return back to minmax.
*/

function deepClone(state){
    var new_state = [];
    for(var idx1 = 0; idx1 < state.length; idx1++) {
        new_state.push(state[idx1].slice(0));
    }
    return new_state;  // return to alphabeta function..
}
//Main class fucntion...
export default Component.extend({
      // four variables to hold the gloabal values to be used in the game.
    playing: false,
    winner: undefined,
    draw: false,
    desktop: true ,


    //Add sound function
    init: function(){  //init function to set constructor super and createjs sounds.
        this._super(...arguments);
        createjs.Sound.registerSound("assets/sounds/click.wav", "place-marker");  //sound when user add a ball in the game grid
   createjs.Sound.registerSound("assets/sounds/drain.wav", "drain");  //sound when user restart the game
   createjs.Sound.registerSound("assets/sounds/cheer.wav", "cheer");  //sound when there is a winner in the game
   createjs.Sound.registerSound("assets/sounds/jazz.wav", "jaz");   // sound when a draw happens in the game
    
   //Mobile variables and function
   var component = this;
   document.addEventListener("deviceready", function(){
     if(shake){
       shake.startWatch(function(){
         component.send('start');
       });
     }
     component.set('desktop', false);
   }, false);
 },
//end init function and mobile

      //didInsertElement function responsible to the game click and stage creation..
    didInsertElement: function() {
        
    var stage = new createjs.Stage(this.$('#stage')[0]); //variable to hold the create js stage value

    //Draw the board
    var board = new createjs.Shape(); // hold the createjs shape value.
    var graphics = board.graphics; //hold the calling board with the graphics value.

    graphics.beginFill('#8e7cc3 '); //responsible to fill the grid lines light purple colour.

    //graphics.drawRect are responsible to create the horizontal seven squares with the coordinates in the correct place. 
    graphics.drawRect(0, 0, 294, 2);   // first 0 to start at the top of the board, second 0 set the gap between lines, 
    graphics.drawRect(0, 100, 294, 2);  //third set where the line finish 294, fourth thickness od the line 2.
    graphics.drawRect(0, 50, 294, 2);  
    graphics.drawRect(0, 150, 294, 2);
    graphics.drawRect(0, 200, 294, 2);
    graphics.drawRect(0, 250, 294, 2);
    graphics.drawRect(0, 300, 294, 2);

     //the same principals as the above but now the horizontal lines are eight and assigns the lins with the coordinates.
     // First coordiants 0 on this is to set to go up teel to start at the top of the board
    graphics.drawRect(0, 0, 2, 300); //Second is to set the 0 for the line goes horizontal
    graphics.drawRect(42, 0, 2, 300); //third is the same like above the thickenss of the line 2
    graphics.drawRect(84, 0, 2, 300);  // Fourth set set where the line finish in the bottom of the board.
    graphics.drawRect(126, 0, 2, 300);
    graphics.drawRect(168, 0, 2, 300);
    graphics.drawRect(210, 0, 2, 300);
    graphics.drawRect(252, 0, 2, 300);
    graphics.drawRect(294, 0, 2, 300);


    board.x = 30;  //The left and right padding to be sett on the board
    board.y = 40;  // The top and bottom padding setting on the board
    board.alpha = 0;
    this.set('board', board);
    
    stage.addChild(board); //Calling the stage to draw the board.

        //Create Markers
        var markers = {
            'x': [],
            'o': []
        }
        for(var x = 0; x < 21; x++) { //For loop to loop 21 times for each x and o the total together 42 filleing the grids
            var circleMarker = new createjs.Shape(); //vairable responsible to create the circle for player o; 
            graphics = circleMarker.graphics;  // adding to the graphics to be drawn 
            graphics.beginFill('#221450'); //adding colour filling 
            graphics.drawCircle(0, 0, 14); // dimension of the circle
            circleMarker.visible = false;  //not visible
            stage.addChild(circleMarker); //adding to stage of the game
            markers.o.push(circleMarker); //using method push() to the markers
   
            var crossMarker = new createjs.Shape(); //variable responsible to create the x for the player ball. 
            graphics = crossMarker.graphics;    // adding to the graphics to be drawn 
            graphics.beginFill('#cc0000');  //adding colour filling 
            graphics.drawCircle(0, 0, 14);  // dimension of the circle
            crossMarker.visible = false;    //not visible
            stage.addChild(crossMarker);   //adding to stage of the game
            markers.x.push(crossMarker);   //using method push() to the markers
          }
   
          this.set('markers', markers);
          this.set('stage', stage,);
   
          //Update the drawing
         // stage.update()
          createjs.Ticker.addEventListener("tick", stage);
    },

    willDestroyElement: function(){  //mobile restarting the game.
        this._super(...arguments);
        if(shake){
          shake.stopWatch();
        }
      },

    click: function(ev) {  // function responsible to the clicks done by the players in the game
        var component = this;

        if(component.get('playing') && !component.get('winner')){  //if statement to get if thersi a player but not winner carry on
            if( ev.offsetX >= 30 
            && ev.offsetY >= 40 && ev.offsetX < 340 
            && ev.offsetY < 340){
                
                var x = Math.floor((ev.offsetX - 40) / 42);
                var y = 5;
                var state = component.get('state');
                while (state[x][y] == 'x' || state[x][y] == 'o'){  //looping of the grids decrescing count by 1 and check which has been filled and place it above.
                    y = y - 1;
                  }

                if(y >= 0){
                    createjs.Sound.play("place-marker");
                    state[x][y] = 'x'; // player x make move array x or y.
                    var move_count = component.get('moves')['x'];   //variable to hold the count moves by the player x
                    var marker = component.get('markers')['x'][move_count];   // holding the value of markers x and the count
                    
                    marker.visible = true;  // set markers to visible on the game
                    marker.x = 50 + x * 42; //markers coodernates wher should appear on the screen
                    marker.y = 70 + y * 50;
                    component.check_winner();  //responsible to check if the user has win..
                    component.get('stage').update();   //if win or not update the stage
                    component.get('moves')['x'] = move_count + 1;   //setting the moves and increase by 1
                    
                    // This function is to set the computer player creating a time break between player and computer player
                    setTimeout(function() {
                        if(!component.get('winner') && !component.get('draw')) {  //if statement to set the computer player similar than the player above. 
                            var move = computer_move(state);

                            state[move.x][move.y] = 'o';
                            marker = component.get('markers')['o'][move_count];
                            move_count = component.get('moves')['o'][move_count];
                            
                            marker.visible = true;
                            marker.x = 50 + move.x * 42;
                            marker.y = 70 + move.y * 50;
                            component.get('moves')['o'] = move_count + 1;
                            component.get('stage').update();
                            component.check_winner();
 
 
                  }
                }, 500); //Time added between each time a player or computer has between each click.
                
            }
            if(!this.get('winner') && window.plugins &&window.plugins.toast) {  //plugins to show a screen pop up at bottom of the game.
             window.plugins.toast.showShortBottom(this.get('player').toUpperCase()+ '_red_ball, O_blue_ball'); //pop up msg x player to play next 
             }
           }
       }
      }, //end click function


      check_winner: function() {

        var state = this.get('state');
        var winner = check_game_winner(state);
        if(winner !== undefined) {  //check if there is a winner or not
          createjs.Sound.play("cheer");  //sound if someone win the game

            if(winner === '') { //check if there is no winner and happena draw
                this.set('draw', true);
                createjs.Sound.play("jaz"); //sound if the game is draw

            } else {
                this.set('winner', winner);  //othewrwise set the winner..
            }
        }
    },


    actions: { //actions is responsible to enable the user interact with the game from interface buttons. like start and so on.. 
      
        start: function() {  //start function responsible to make the functionality of the button start, restart on the game interface.
          
         if(window.plugins && window.plugins.toast) {
            window.plugins.toast.showShortBottom('Game Start');
          } 
 
          var board = this.get('board');  // set the game board 
          board.alpha = 0; // set board to 0
 
          //createjs.Tween.get(board).to({alpha: 1}, 1000)
          if(this.get('playing')){
            var markers = this.get('markers');
            for(var idx = 0; idx < 5; idx++){
              createjs.Tween.get(markers.x[idx]).to({y: 600}, 500);
              createjs.Tween.get(markers.o[idx]).to({y: 600}, 500);
            }
            createjs.Sound.play("drain");
            createjs.Tween.get(board).wait(500).to({alpha: 1}, 1000)
          }else{
            createjs.Tween.get(board).to({alpha: 1}, 1000)
          }
          this.set('playing', true); //set playing start true
          this.set('winner', undefined);  //set winner is undefined.
          this.set('draw', undefined);  // set draw undefined
          this.set('state', [   //set game state 
            [undefined, undefined, undefined, undefined, undefined, undefined], //board square not clicked yet.
            [undefined, undefined, undefined, undefined, undefined, undefined],
            [undefined, undefined, undefined, undefined, undefined, undefined],
            [undefined, undefined, undefined, undefined, undefined, undefined],
            [undefined, undefined, undefined, undefined, undefined, undefined],
            [undefined, undefined, undefined, undefined, undefined, undefined],
            [undefined, undefined, undefined, undefined, undefined, undefined],
          ]);
          this.set('moves', {'x': 0, 'o': 0}); //set grid 0 for both players
          this.set('player', 'x');        // set first player x user
          var call_markers = this.get('markers');  //variable to hold values of game markers
          for(var idx4 = 0; idx4 < 21; idx4++) {
            call_markers.x[idx4].visible = false;  //set markers user visibility false
            call_markers.o[idx4].visible = false;  //set markers computer visibility false
          }
          // this.get('stage').update();
        },
 
        
      },
    option1: function(){  //function for easy button on options interface..
        var moves = alphabeta(state, 10, 'o', Number.MIN_VALUE, Number.MAX_VALUE);
        if(moves > 1){
            this.set(moves = 1);
        }
    }

});
