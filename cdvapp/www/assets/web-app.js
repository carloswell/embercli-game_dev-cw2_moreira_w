"use strict";



define('web-app/app', ['exports', 'web-app/resolver', 'ember-load-initializers', 'web-app/config/environment'], function (exports, _resolver, _emberLoadInitializers, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var App = Ember.Application.extend({
    modulePrefix: _environment.default.modulePrefix,
    podModulePrefix: _environment.default.podModulePrefix,
    Resolver: _resolver.default
  });

  (0, _emberLoadInitializers.default)(App, _environment.default.modulePrefix);

  exports.default = App;
});
define('web-app/components/connect-four', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  //import ember framework for the game.

  function check_game_winner(state) {
    //function called when the check_winner counter is playing.

    var patterns = [// patterns array variable to hold the values for horizontal,vertical and diagonal partterns for winning

    //horizontal win partner

    //1-horizontal row
    [[0, 0], [1, 0], [2, 0], [3, 0]], [[1, 0], [2, 0], [3, 0], [4, 0]], [[2, 0], [3, 0], [4, 0], [5, 0]], [[3, 0], [4, 0], [5, 0], [6, 0]],
    //2-horizontal row
    [[0, 1], [1, 1], [2, 1], [3, 1]], [[1, 1], [2, 1], [3, 1], [4, 1]], [[2, 1], [3, 1], [4, 1], [5, 1]], [[3, 1], [4, 1], [5, 1], [6, 1]],
    //3-horizontal row
    [[0, 2], [1, 2], [2, 2], [3, 2]], [[1, 2], [2, 2], [3, 2], [4, 2]], [[2, 2], [3, 2], [4, 2], [5, 2]], [[3, 2], [4, 2], [5, 2], [6, 2]],
    //4-horizontal row
    [[0, 3], [1, 3], [2, 3], [3, 3]], [[1, 3], [2, 3], [3, 3], [4, 3]], [[2, 3], [3, 3], [4, 3], [5, 3]], [[3, 3], [4, 3], [5, 3], [6, 3]],
    //5-horizontal row
    [[0, 4], [1, 4], [2, 4], [3, 4]], [[1, 4], [2, 4], [3, 4], [4, 4]], [[2, 4], [3, 4], [4, 4], [5, 4]], [[3, 4], [4, 4], [5, 4], [6, 4]],
    //6- horizontal row
    [[0, 5], [1, 5], [2, 5], [3, 5]], [[1, 5], [2, 5], [3, 5], [4, 5]], [[2, 5], [3, 5], [4, 5], [5, 5]], [[3, 5], [4, 5], [5, 5], [6, 5]],

    //vertical win partner

    //1-vertical column  
    [[0, 0], [0, 1], [0, 2], [0, 3]], [[0, 1], [0, 2], [0, 3], [0, 4]], [[0, 2], [0, 3], [0, 4], [0, 5]],
    //2-vertical column
    [[1, 0], [1, 1], [1, 2], [1, 3]], [[1, 1], [1, 2], [1, 3], [1, 4]], [[1, 2], [1, 3], [1, 4], [1, 5]],
    //3-vertical column
    [[2, 0], [2, 1], [2, 2], [2, 3]], [[2, 1], [2, 2], [2, 3], [2, 4]], [[2, 2], [2, 3], [2, 4], [2, 5]],
    //4-vertical column
    [[3, 0], [3, 1], [3, 2], [3, 3]], [[3, 1], [3, 2], [3, 3], [3, 4]], [[3, 2], [3, 3], [3, 4], [3, 5]],
    //5-vertical column
    [[4, 0], [4, 1], [4, 2], [4, 3]], [[4, 1], [4, 2], [4, 3], [4, 4]], [[4, 2], [4, 3], [4, 4], [4, 5]],
    //6-vertical column
    [[5, 0], [5, 1], [5, 2], [5, 3]], [[5, 1], [5, 2], [5, 3], [5, 4]], [[5, 2], [5, 3], [5, 4], [5, 5]],
    //7-vertical column
    [[6, 0], [6, 1], [6, 2], [6, 3]], [[6, 1], [6, 2], [6, 3], [6, 4]], [[6, 2], [6, 3], [6, 4], [6, 5]],

    // Diagonal pattern wins   

    //Left to right diagonal..
    [[0, 3], [1, 2], [2, 1], [3, 0]], [[0, 4], [1, 3], [2, 2], [3, 1]], [[1, 4], [2, 3], [3, 2], [4, 1]], [[1, 3], [2, 2], [3, 1], [4, 0]], [[0, 5], [1, 4], [2, 3], [3, 2]], [[1, 4], [2, 3], [3, 2], [4, 1]], [[2, 3], [3, 2], [4, 1], [5, 0]], [[1, 5], [2, 4], [3, 3], [4, 2]], [[2, 4], [3, 3], [4, 2], [5, 1]], [[3, 3], [4, 2], [5, 1], [6, 0]], [[2, 5], [3, 4], [4, 3], [5, 2]], [[3, 4], [4, 3], [5, 2], [6, 1]], [[3, 5], [4, 4], [5, 3], [6, 2]],
    //right to left diagonal
    [[3, 0], [4, 1], [5, 2], [6, 3]], [[2, 0], [3, 1], [4, 2], [5, 3]], [[3, 1], [4, 2], [5, 3], [6, 4]], [[1, 0], [2, 1], [3, 2], [4, 3]], [[2, 1], [3, 2], [4, 3], [5, 4]], [[3, 2], [4, 3], [5, 4], [6, 5]], [[0, 0], [1, 1], [2, 2], [3, 3]], [[1, 1], [2, 2], [3, 3], [4, 4]], [[2, 2], [3, 3], [4, 4], [5, 5]], [[0, 1], [1, 2], [2, 3], [3, 4]], [[1, 2], [2, 3], [3, 4], [4, 5]], [[0, 2], [1, 3], [2, 4], [3, 5]]];
    //for loop is responsible to loope over the patterns arrays searching for a winner

    for (var ids = 0; ids < patterns.length; ids++) {
      var pattern = patterns[ids];
      var winner = state[pattern[0][0]][pattern[0][1]]; //check the state on begging of each pattern to make sure eash square has the same state.
      if (winner) {
        // if there is a winner return winner otherwise
        for (var idx = 1; idx < pattern.length; idx++) {
          if (winner != state[pattern[idx][0]][pattern[idx][1]]) {
            winner = undefined; //if not winner break and start again.
            break;
          }
        }
        if (winner) {
          return winner;
        }
      }
    }

    // varaible to hold the value of game draw
    var draw = true;
    for (var x = 0; x <= 2; x++) {
      //looping by each square check if none of them are empty
      for (var y = 0; y <= 2; y++) {
        if (!state[x][y]) {
          //if is not empty game continue otherwise return to check_winner and the message. 
          return undefined;
        }
      }
    }
    return '';
  } //end of check_game_winner function

  /*DeepClone function is responsible to store the game trees by use of for loop 
  *and story inside a array, to return back to minmax.
  */

  function deepClone(state) {
    var new_state = [];
    for (var idx1 = 0; idx1 < state.length; idx1++) {
      new_state.push(state[idx1].slice(0));
    }
    return new_state; // return to alphabeta function..
  }

  //Main class fucntion...
  exports.default = Ember.Component.extend({

    // four variables to hold the gloabal values to be used in the game.
    playing: false,
    winner: undefined,
    draw: false,
    desktop: true,

    init: function init() {
      //init function to set constructor super and createjs sounds.
      this._super.apply(this, arguments);
      createjs.Sound.registerSound("assets/sounds/click.wav", "place-marker"); //sound when user add a ball in the game grid
      createjs.Sound.registerSound("assets/sounds/drain.wav", "drain"); //sound when user restart the game
      createjs.Sound.registerSound("assets/sounds/cheer.wav", "cheer"); //sound when there is a winner in the game
      createjs.Sound.registerSound("assets/sounds/jazz.wav", "jaz"); // sound when a draw happens in the game

      //Mobile variables and function
      var component = this;
      document.addEventListener("deviceready", function () {
        if (shake) {
          shake.startWatch(function () {
            component.send('start');
          });
        }
        component.set('desktop', false);
      }, false);
    },
    //end mobile

    //didInsertElement function responsible to the game click and stage creation..
    didInsertElement: function didInsertElement() {

      var stage = new createjs.Stage(this.$('#stage')[0]); //variable to hold the create js stage value

      //Draw the board
      var board = new createjs.Shape(); // hold the createjs shape value.
      var graphics = board.graphics; //hold the calling board with the graphics value.

      graphics.beginFill('#8e7cc3 '); //responsible to fill the grid lines light purple colour.

      //graphics.drawRect are responsible to create the horizontal seven squares with the coordinates in the correct place. 
      graphics.drawRect(0, 0, 294, 2); // first 0 to start at the top of the board, second 0 set the gap between lines, 
      graphics.drawRect(0, 100, 294, 2); //third set where the line finish 294, fourth thickness od the line 2.
      graphics.drawRect(0, 50, 294, 2);
      graphics.drawRect(0, 150, 294, 2);
      graphics.drawRect(0, 200, 294, 2);
      graphics.drawRect(0, 250, 294, 2);
      graphics.drawRect(0, 300, 294, 2);

      //the same principals as the above but now the horizontal lines are eight and assigns the lins with the coordinates.
      // First coordiants 0 on this is to set to go up teel to start at the top of the board
      graphics.drawRect(0, 0, 2, 300); //Second is to set the 0 for the line goes horizontal
      graphics.drawRect(42, 0, 2, 300); //third is the same like above the thickenss of the line 2
      graphics.drawRect(84, 0, 2, 300); // Fourth set set where the line finish in the bottom of the board.
      graphics.drawRect(126, 0, 2, 300);
      graphics.drawRect(168, 0, 2, 300);
      graphics.drawRect(210, 0, 2, 300);
      graphics.drawRect(252, 0, 2, 300);
      graphics.drawRect(294, 0, 2, 300);

      board.x = 30; //The left and right padding to be sett on the board
      board.y = 40; // The top and bottom padding setting on the board
      board.alpha = 0;
      this.set('board', board);

      stage.addChild(board); //Calling the stage to draw the board.


      var markers = { //Variable array to hold the key values of x and o.
        'x': [],
        'o': []
      };

      for (var x = 0; x < 21; x++) {
        //For loop to loop 21 times for each x and o the total together 42 filleing the grids
        var circleMarker = new createjs.Shape(); //vairable responsible to create the circle for player o; 
        graphics = circleMarker.graphics; // adding to the graphics to be drawn 
        graphics.beginFill('#221450'); //adding colour filling 
        graphics.drawCircle(0, 0, 14); // dimension of the circle
        circleMarker.visible = false; //not visible
        stage.addChild(circleMarker); //adding to stage of the game
        markers.o.push(circleMarker); //using method push() to the markers

        var crossMarker = new createjs.Shape(); //variable responsible to create the x for the player ball. 
        graphics = crossMarker.graphics; // adding to the graphics to be drawn 
        graphics.beginFill('#cc0000'); //adding colour filling 
        graphics.drawCircle(0, 0, 14); // dimension of the circle
        crossMarker.visible = false; //not visible
        stage.addChild(crossMarker); //adding to stage of the game
        markers.x.push(crossMarker); //using method push() to the markers
      }

      this.set('markers', markers);
      this.set('stage', stage);

      //Update the drawing
      // stage.update()
      createjs.Ticker.addEventListener("tick", stage);
    },

    willDestroyElement: function willDestroyElement() {
      //mobile restarting the game.
      this._super.apply(this, arguments);
      if (shake) {
        shake.stopWatch();
      }
    },

    click: function click(ev) {
      // function responsible to the clicks done by the players in the game

      var component = this;
      if (component.get('playing') && !component.get('winner')) {
        //if statement to get if thersi a player but not winner carry on
        if (ev.offsetX >= 30 && ev.offsetY >= 40 && ev.offsetX < 340 && ev.offsetY < 340) {
          //to set the x and y axes  30, 340 for x axe and 40, 340 for y axe
          var x = Math.floor((ev.offsetX - 40) / 42); //variable x hold value of x counter places 
          var y = 5; //variable y to hold number of rows
          var state = component.get('state');
          while (state[x][y] == 'x' || state[x][y] == 'o') {
            //looping of the grids decrescing count by 1 and check which has been filled and place it above.
            y = y - 1;
          }

          if (y >= 0) {
            //if statement responsible to check coordinates from the squares above and place it.
            createjs.Sound.play("place-marker"); //sound of player make a moving.
            state[x][y] = 'x'; // player x make move array x or y.
            var move_count = component.get('moves')['x']; //variable to hold the count moves by the player x
            var marker = component.get('markers')['x'][move_count]; // holding the value of markers x and the count
            marker.visible = true; // set markers to visible on the game
            marker.x = 50 + x * 42; //markers coodernates wher should appear on the screen
            marker.y = 70 + y * 50;

            component.check_winner(); //responsible to check if the user has win..
            component.get('stage').update(); //if win or not update the stage
            component.get('moves')['x'] = move_count + 1; //setting the moves and increase by 1

            // This function is to set the computer player creating a time break between player and computer player
            setTimeout(function () {

              if (!component.get('winner') && !component.get('draw')) {
                //if statement to set the computer player similar than the player above. 
                createjs.Sound.play("place-marker");
                var move = component.computer_move(state);

                state[move.x][move.y] = 'o';
                marker = component.get('markers')['o'][move_count];
                move_count = component.get('moves')['o'][move_count];

                marker.visible = true;

                marker.x = 50 + move.x * 42;
                marker.y = 70 + move.y * 50;
                component.get('moves')['o'] = move_count + 1;

                component.get('stage').update();

                component.check_winner();
              }
            }, 500); //Time added between each time a player or computer has between each click.
          }
          if (!this.get('winner') && window.plugins && window.plugins.toast) {
            //plugins to show a screen pop up at bottom of the game.
            window.plugins.toast.showShortBottom(this.get('player').toUpperCase() + '_red_ball, O_blue_ball'); //pop up msg x player to play next 
          }
        }
      }
    }, //end click function

    //function responsible to check if after each player clicks if there is a winner or not.
    check_winner: function check_winner() {

      var state = this.get('state');
      var winner = check_game_winner(state);
      if (winner !== undefined) {
        //check if there is a winner or not
        createjs.Sound.play("cheer"); //sound if someone win the game

        if (winner === '') {
          //check if there is no winner and happena draw
          this.set('draw', true);
          createjs.Sound.play("jaz"); //sound if the game is draw
        } else {
          this.set('winner', winner); //othewrwise set the winner..
        }
      }
    },

    //Function responsible to calculate the computer moves make proactive.
    computer_move: function computer_move(state) {
      function alphabeta(state, limit, player, alpha, beta) {
        // function responsible to use heuristic algorithm, helping the computer pruning the squares and make his moves more inteligent
        var moves = [];
        if (limit > 0) {
          //if statement to get parameter limit and if the same is greater than 0.

          if (player === 'o') {
            //if computer is the player find minimum and maximo value on alpahbeta pruning
            var cutoff = Number.MIN_VALUE;
          } else {
            var cutoff = Number.MAX_VALUE;
          }
          for (var idx2 = 5; idx2 >= 0; idx2--) {
            // nested for looping to find the squares that are empty and add as possible move
            for (var idx1 = 0; idx1 <= 6; idx1++) {
              if (state[idx1][idx2] === undefined) {
                var move = {
                  x: idx1,
                  y: idx2,
                  state: deepClone(state),
                  score: 0
                };
                move.state[idx1][idx2] = player; //The move created by taked the coordentates is passed to deepClone function to store and make used of this move further.

                if (limit === 1 || check_game_winner(move.state) !== undefined) {
                  //if statement to check function check_game_winner and creates a score

                  if (check_game_winner(move.state) !== undefined) {
                    var winner = check_game_winner(move.state);

                    if (winner === 'o') {
                      //If winner is the computer the score is set to 1000
                      move.score = 1000;
                    } else if (winner === 'x') {
                      //if winner is the user player the score is set to -1000
                      move.score = -1000;
                    }
                  }
                } else {
                  //otherwise loop again and with if statement to come up with other score after a move has be done 
                  move.moves = alphabeta(move.state, limit - 1, player == 'x' ? 'o' : 'x', alpha, beta);
                  var score = undefined;
                  for (var idx3 = 0; idx3 < move.moves.length; idx3++) {
                    if (score === undefined) {
                      score = move.moves[idx3].score;
                    } else if (player === 'x') {
                      score = Math.max(score, move.moves[idx3].score);
                    } else if (player === 'o') {
                      score = Math.min(score, move.moves[idx3].score);
                    }
                  }
                  move.score = score;
                }
                moves.push(move); //after moves score calculated push move back to computer_move function to store possible moves.
                if (player === 'o') {
                  // if statement to calcualte min as beta and max as alpha . 
                  cutoff = Math.max(cutoff, move.score);
                  alpha = Math.max(cutoff, alpha);
                } else {
                  cutoff = Math.min(cutoff, move.score);
                  beta = Math.min(cutoff, beta);
                }
                if (beta <= alpha) {//if beta is less or equal to alpha return moves.
                  //return moves;

                }
              }
            }
          }
        }
        return moves; //return moves to deepClone function.
      }

      var moves = alphabeta(state, 2, 'o', Number.MIN_VALUE, Number.MAX_VALUE); //change the limit on alphabeta function  making the computer hard to bit.
      var max_score = undefined;
      var move = undefined;

      for (var idx = 0; idx < moves.length; idx++) {
        //for loop to looping over the possible scores to find highest and pass the move
        if (max_score === undefined || moves[idx].score > max_score) {
          max_score = moves[idx].score;
          move = {
            x: moves[idx].x, //set coordentates x for the score  and pas to the counter
            y: moves[idx].y //set coordentates y for the score  and pas to the counter
          };
        }
      }
      return move; //return move to deepClone()
    },
    //end of computer_move()

    actions: { //actions is responsible to enable the user interact with the game from interface buttons. like start and so on.. 

      start: function start() {
        //start function responsible to make the functionality of the button start, restart on the game interface.

        if (window.plugins && window.plugins.toast) {
          window.plugins.toast.showShortBottom('Game Start');
        }

        var board = this.get('board'); // set the game board 
        board.alpha = 0; // set board to 0

        //createjs.Tween.get(board).to({alpha: 1}, 1000)
        if (this.get('playing')) {
          var markers = this.get('markers');
          for (var idx = 0; idx < 5; idx++) {
            createjs.Tween.get(markers.x[idx]).to({ y: 600 }, 500);
            createjs.Tween.get(markers.o[idx]).to({ y: 600 }, 500);
          }
          createjs.Sound.play("drain");
          createjs.Tween.get(board).wait(500).to({ alpha: 1 }, 1000);
        } else {
          createjs.Tween.get(board).to({ alpha: 1 }, 1000);
        }
        this.set('playing', true); //set playing start true
        this.set('winner', undefined); //set winner is undefined.
        this.set('draw', undefined); // set draw undefined
        this.set('state', [//set game state 
        [undefined, undefined, undefined, undefined, undefined, undefined], //board sqaure not clicked yet.
        [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined], [undefined, undefined, undefined, undefined, undefined, undefined]]);
        this.set('moves', { 'x': 0, 'o': 0 }); //set grid 0 for both players
        this.set('player', 'x'); // set first player x user
        var call_markers = this.get('markers'); //variable to hold values of game markers
        for (var idx4 = 0; idx4 < 21; idx4++) {
          call_markers.x[idx4].visible = false; //set markers user visibility false
          call_markers.o[idx4].visible = false; //set markers computer visibility false
        }
        // this.get('stage').update();
      },

      option_1: function option_1() {
        //function for easy button on options interface..
        var easy = 1;
      }

    }

  });
});
define('web-app/components/welcome-page', ['exports', 'ember-welcome-page/components/welcome-page'], function (exports, _welcomePage) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _welcomePage.default;
    }
  });
});
define('web-app/helpers/app-version', ['exports', 'web-app/config/environment', 'ember-cli-app-version/utils/regexp'], function (exports, _environment, _regexp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.appVersion = appVersion;
  function appVersion(_) {
    var hash = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    var version = _environment.default.APP.version;
    // e.g. 1.0.0-alpha.1+4jds75hf

    // Allow use of 'hideSha' and 'hideVersion' For backwards compatibility
    var versionOnly = hash.versionOnly || hash.hideSha;
    var shaOnly = hash.shaOnly || hash.hideVersion;

    var match = null;

    if (versionOnly) {
      if (hash.showExtended) {
        match = version.match(_regexp.versionExtendedRegExp); // 1.0.0-alpha.1
      }
      // Fallback to just version
      if (!match) {
        match = version.match(_regexp.versionRegExp); // 1.0.0
      }
    }

    if (shaOnly) {
      match = version.match(_regexp.shaRegExp); // 4jds75hf
    }

    return match ? match[0] : version;
  }

  exports.default = Ember.Helper.helper(appVersion);
});
define('web-app/helpers/pluralize', ['exports', 'ember-inflector/lib/helpers/pluralize'], function (exports, _pluralize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _pluralize.default;
});
define('web-app/helpers/singularize', ['exports', 'ember-inflector/lib/helpers/singularize'], function (exports, _singularize) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _singularize.default;
});
define('web-app/initializers/app-version', ['exports', 'ember-cli-app-version/initializer-factory', 'web-app/config/environment'], function (exports, _initializerFactory, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var name = void 0,
      version = void 0;
  if (_environment.default.APP) {
    name = _environment.default.APP.name;
    version = _environment.default.APP.version;
  }

  exports.default = {
    name: 'App Version',
    initialize: (0, _initializerFactory.default)(name, version)
  };
});
define('web-app/initializers/container-debug-adapter', ['exports', 'ember-resolver/resolvers/classic/container-debug-adapter'], function (exports, _containerDebugAdapter) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'container-debug-adapter',

    initialize: function initialize() {
      var app = arguments[1] || arguments[0];

      app.register('container-debug-adapter:main', _containerDebugAdapter.default);
      app.inject('container-debug-adapter:main', 'namespace', 'application:main');
    }
  };
});
define('web-app/initializers/data-adapter', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'data-adapter',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('web-app/initializers/ember-data', ['exports', 'ember-data/setup-container', 'ember-data'], function (exports, _setupContainer) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'ember-data',
    initialize: _setupContainer.default
  };
});
define('web-app/initializers/export-application-global', ['exports', 'web-app/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.initialize = initialize;
  function initialize() {
    var application = arguments[1] || arguments[0];
    if (_environment.default.exportApplicationGlobal !== false) {
      var theGlobal;
      if (typeof window !== 'undefined') {
        theGlobal = window;
      } else if (typeof global !== 'undefined') {
        theGlobal = global;
      } else if (typeof self !== 'undefined') {
        theGlobal = self;
      } else {
        // no reasonable global, just bail
        return;
      }

      var value = _environment.default.exportApplicationGlobal;
      var globalName;

      if (typeof value === 'string') {
        globalName = value;
      } else {
        globalName = Ember.String.classify(_environment.default.modulePrefix);
      }

      if (!theGlobal[globalName]) {
        theGlobal[globalName] = application;

        application.reopen({
          willDestroy: function willDestroy() {
            this._super.apply(this, arguments);
            delete theGlobal[globalName];
          }
        });
      }
    }
  }

  exports.default = {
    name: 'export-application-global',

    initialize: initialize
  };
});
define('web-app/initializers/injectStore', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'injectStore',
    before: 'store',
    initialize: function initialize() {}
  };
});
define('web-app/initializers/store', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'store',
    after: 'ember-data',
    initialize: function initialize() {}
  };
});
define('web-app/initializers/transforms', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: 'transforms',
    before: 'store',
    initialize: function initialize() {}
  };
});
define("web-app/instance-initializers/ember-data", ["exports", "ember-data/initialize-store-service"], function (exports, _initializeStoreService) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = {
    name: "ember-data",
    initialize: _initializeStoreService.default
  };
});
define('web-app/resolver', ['exports', 'ember-resolver'], function (exports, _emberResolver) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = _emberResolver.default;
});
define('web-app/router', ['exports', 'web-app/config/environment'], function (exports, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });


  var Router = Ember.Router.extend({
    location: _environment.default.locationType,
    rootURL: _environment.default.rootURL
  });

  Router.map(function () {
    this.route('game', { path: '/' });
  });

  exports.default = Router;
});
define('web-app/services/ajax', ['exports', 'ember-ajax/services/ajax'], function (exports, _ajax) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  Object.defineProperty(exports, 'default', {
    enumerable: true,
    get: function () {
      return _ajax.default;
    }
  });
});
define("web-app/templates/application", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "po+39QAD", "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[6,\"section\"],[9,\"id\",\"game\"],[7],[0,\"\\n  \"],[6,\"header\"],[7],[0,\"\\n    \"],[6,\"h1\"],[7],[4,\"link-to\",[\"game\"],null,{\"statements\":[[6,\"img\"],[9,\"src\",\"assets/images/connect-4.png\"],[9,\"alt\",\"logo\"],[7],[8]],\"parameters\":[]},null],[8],[0,\"  \"],[0,\"\\n  \"],[8],[0,\"\\n  \"],[6,\"article\"],[7],[0,\"\\n    \"],[1,[18,\"outlet\"],false],[0,\"  \"],[0,\"\\n  \"],[8],[0,\"\\n  \"],[6,\"footer\"],[7],[0,\" \"],[0,\"\\n    \"],[6,\"div\"],[9,\"class\",\"float-left\"],[7],[0,\"\\n      \"],[6,\"p\"],[7],[0,\"Powered by Ember.\"],[8],[0,\"\\n      \"],[6,\"p\"],[7],[0,\"Moreira W 24359939 \"],[8],[0,\"\\n\\n    \"],[8],[0,\"\\n    \"],[6,\"div\"],[9,\"class\",\"float-right\"],[7],[0,\"\\n        \"],[6,\"p\"],[7],[0,\"Please shake the Device\"],[8],[0,\"\\n        \"],[6,\"p\"],[7],[0,\"to start or restart...\"],[8],[0,\"\\n    \"],[8],[0,\"\\n    \\n  \"],[8],[0,\"\\n\"],[8],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "web-app/templates/application.hbs" } });
});
define("web-app/templates/components/connect-four", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "2VR9LhcH", "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[4,\"if\",[[20,[\"playing\"]]],null,{\"statements\":[[4,\"if\",[[20,[\"winner\"]]],null,{\"statements\":[[0,\"    \"],[6,\"div\"],[7],[0,\"\\n      \"],[1,[18,\"winner\"],false],[0,\" won!\\n    \"],[8],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"\\n\"],[4,\"if\",[[20,[\"draw\"]]],null,{\"statements\":[[0,\"        \"],[6,\"div\"],[7],[0,\"we'll call it a draw\"],[8],[0,\"\\n\"]],\"parameters\":[]},null],[4,\"if\",[[20,[\"desktop\"]]],null,{\"statements\":[[0,\"    \"],[6,\"button\"],[9,\"class\",\"btn2\"],[3,\"action\",[[19,0,[]],\"start\"]],[7],[0,\"Restart\"],[8],[0,\"\\n\"]],\"parameters\":[]},null]],\"parameters\":[]},{\"statements\":[[4,\"if\",[[20,[\"desktop\"]]],null,{\"statements\":[[0,\"      \"],[6,\"button\"],[9,\"class\",\"start\"],[3,\"action\",[[19,0,[]],\"start\"]],[7],[0,\"Start\"],[8],[0,\"\\n      \"],[6,\"div\"],[9,\"class\",\"#\"],[9,\"style\",\"float: right;\"],[7],[0,\"\\n      \"],[6,\"option\"],[9,\"class\",\"text-center\"],[7],[0,\"Options\"],[8],[0,\"\\n        \"],[6,\"button\"],[9,\"class\",\"btn\"],[3,\"action\",[[19,0,[]],\"option_1\"]],[7],[0,\"Easy\"],[8],[0,\"\\n        \"],[6,\"button\"],[9,\"class\",\"btn\"],[3,\"action\",[[19,0,[]],\"option_2\"]],[7],[0,\"Medium\"],[8],[0,\"\\n        \"],[6,\"button\"],[9,\"class\",\"btn\"],[3,\"action\",[[19,0,[]],\"option_3\"]],[7],[0,\"Hard\"],[8],[0,\"\\n      \"],[8],[0,\"\\n\"]],\"parameters\":[]},null],[0,\"    \\n\"]],\"parameters\":[]}],[0,\"\\n\\n\"],[6,\"canvas\"],[9,\"id\",\"stage\"],[9,\"width\",\"380\"],[9,\"height\",\"380\"],[7],[8],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "web-app/templates/components/connect-four.hbs" } });
});
define("web-app/templates/game", ["exports"], function (exports) {
  "use strict";

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = Ember.HTMLBars.template({ "id": "gAL+yVfY", "block": "{\"symbols\":[],\"statements\":[[0,\"\\n\"],[0,\"\\n\\n\"],[1,[18,\"connect-four\"],false],[0,\"\\n\"]],\"hasEval\":false}", "meta": { "moduleName": "web-app/templates/game.hbs" } });
});


define('web-app/config/environment', [], function() {
  var prefix = 'web-app';
try {
  var metaName = prefix + '/config/environment';
  var rawConfig = document.querySelector('meta[name="' + metaName + '"]').getAttribute('content');
  var config = JSON.parse(unescape(rawConfig));

  var exports = { 'default': config };

  Object.defineProperty(exports, '__esModule', { value: true });

  return exports;
}
catch(err) {
  throw new Error('Could not read config from meta tag with name "' + metaName + '".');
}

});

if (!runningTests) {
  require("web-app/app")["default"].create({"name":"web-app","version":"0.0.0+138b1692"});
}
//# sourceMappingURL=web-app.map
