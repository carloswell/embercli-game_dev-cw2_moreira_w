'use strict';

define('web-app/tests/app.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | app');

  QUnit.test('app.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'app.js should pass ESLint\n\n6:13 - Use import Application from \'@ember/application\'; instead of using Ember.Application (ember/new-module-imports)');
  });

  QUnit.test('components/connect-four.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'components/connect-four.js should pass ESLint\n\n122:9 - \'draw\' is assigned a value but never used. (no-unused-vars)\n146:16 - Use import Component from \'@ember/component\'; instead of using Ember.Component (ember/new-module-imports)\n156:4 - \'createjs\' is not defined. (no-undef)\n157:4 - \'createjs\' is not defined. (no-undef)\n158:4 - \'createjs\' is not defined. (no-undef)\n159:4 - \'createjs\' is not defined. (no-undef)\n164:9 - \'shake\' is not defined. (no-undef)\n165:8 - \'shake\' is not defined. (no-undef)\n177:21 - \'createjs\' is not defined. (no-undef)\n180:22 - \'createjs\' is not defined. (no-undef)\n220:33 - \'createjs\' is not defined. (no-undef)\n228:32 - \'createjs\' is not defined. (no-undef)\n242:8 - \'createjs\' is not defined. (no-undef)\n248:11 - \'shake\' is not defined. (no-undef)\n249:10 - \'shake\' is not defined. (no-undef)\n266:15 - \'createjs\' is not defined. (no-undef)\n282:20 - \'createjs\' is not defined. (no-undef)\n317:11 - \'createjs\' is not defined. (no-undef)\n321:17 - \'createjs\' is not defined. (no-undef)\n338:17 - \'cutoff\' is already defined. (no-redeclare)\n432:14 - \'createjs\' is not defined. (no-undef)\n433:14 - \'createjs\' is not defined. (no-undef)\n435:12 - \'createjs\' is not defined. (no-undef)\n436:12 - \'createjs\' is not defined. (no-undef)\n438:12 - \'createjs\' is not defined. (no-undef)\n463:14 - \'easy\' is assigned a value but never used. (no-unused-vars)');
  });

  QUnit.test('resolver.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'resolver.js should pass ESLint\n\n');
  });

  QUnit.test('router.js', function (assert) {
    assert.expect(1);
    assert.ok(false, 'router.js should pass ESLint\n\n4:16 - Use import EmberRouter from \'@ember/routing/router\'; instead of using Ember.Router (ember/new-module-imports)');
  });
});
define('web-app/tests/helpers/destroy-app', ['exports'], function (exports) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = destroyApp;
  function destroyApp(application) {
    Ember.run(application, 'destroy');
  }
});
define('web-app/tests/helpers/module-for-acceptance', ['exports', 'qunit', 'web-app/tests/helpers/start-app', 'web-app/tests/helpers/destroy-app'], function (exports, _qunit, _startApp, _destroyApp) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });

  exports.default = function (name) {
    var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

    (0, _qunit.module)(name, {
      beforeEach: function beforeEach() {
        this.application = (0, _startApp.default)();

        if (options.beforeEach) {
          return options.beforeEach.apply(this, arguments);
        }
      },
      afterEach: function afterEach() {
        var _this = this;

        var afterEach = options.afterEach && options.afterEach.apply(this, arguments);
        return Ember.RSVP.resolve(afterEach).then(function () {
          return (0, _destroyApp.default)(_this.application);
        });
      }
    });
  };
});
define('web-app/tests/helpers/start-app', ['exports', 'web-app/app', 'web-app/config/environment'], function (exports, _app, _environment) {
  'use strict';

  Object.defineProperty(exports, "__esModule", {
    value: true
  });
  exports.default = startApp;
  function startApp(attrs) {
    var attributes = Ember.merge({}, _environment.default.APP);
    attributes.autoboot = true;
    attributes = Ember.merge(attributes, attrs); // use defaults, but you can override;

    return Ember.run(function () {
      var application = _app.default.create(attributes);
      application.setupForTesting();
      application.injectTestHelpers();
      return application;
    });
  }
});
define('web-app/tests/test-helper', ['web-app/app', 'web-app/config/environment', '@ember/test-helpers', 'ember-qunit'], function (_app, _environment, _testHelpers, _emberQunit) {
  'use strict';

  (0, _testHelpers.setApplication)(_app.default.create(_environment.default.APP));

  (0, _emberQunit.start)();
});
define('web-app/tests/tests.lint-test', [], function () {
  'use strict';

  QUnit.module('ESLint | tests');

  QUnit.test('helpers/destroy-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/destroy-app.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/module-for-acceptance.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/module-for-acceptance.js should pass ESLint\n\n');
  });

  QUnit.test('helpers/start-app.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'helpers/start-app.js should pass ESLint\n\n');
  });

  QUnit.test('test-helper.js', function (assert) {
    assert.expect(1);
    assert.ok(true, 'test-helper.js should pass ESLint\n\n');
  });
});
require('web-app/tests/test-helper');
EmberENV.TESTS_FILE_LOADED = true;
//# sourceMappingURL=tests.map
